describe 'Parenthino' do
  def evaluate(code)
    Parenthino::Compiler.eval code
  end

  it 'compiles false and true literals' do
    evaluate(%q|#t|).should eq true
    evaluate(%q|#f|).should eq false
  end

  it 'compiles empty program' do
    evaluate(%q||).should == nil
  end

  it 'compiles integer literals' do
    evaluate(%q|2|).should == 2
    evaluate(%q|2934|).should == 2934
    evaluate(%q|+2|).should == 2
    evaluate(%q|-4|).should == -4
  end

  it 'compiles real literals' do
    evaluate(%q|2.4|).should == 2.4
    evaluate(%q|4.|).should == 4.0
  end
end
