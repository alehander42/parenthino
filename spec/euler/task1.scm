;If we list all the natural numbers below 10 that are multiples
; of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

;Find the sum of all the multiples of 3 or 5 below 1000.

(define (sum-multiples-of-below numbers largest) 
  (reduce 0 + (filter 
  	            (lambda (num)
  	            	(any
  	            		(lambda (number)
  	            			(+ 0 rem num number)) numbers))
  	            (range 1 largest 1))))