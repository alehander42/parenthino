(define (parse code)
  (.Parenthino::Parser.parse code))

(it "parses true and false literals"
  [assert-eq (parse "#t") '(boolean #t (0 0))]
  [assert-eq (parse "#f") '(boolean #f (0 0))]
)

