describe Parenthino::Parser do
  def parse(code)
    Parenthino::Parser.parse code
  end

  it 'parses true and false literals' do
    parse(%q|#t|).should eq [[:boolean, true, [0, 0]]]
    parse(%q|#f|).should eq [[:boolean, false, [0, 0]]]
  end

  it 'parses empty program' do
    parse(%q||).should eq []
  end

  it 'parses a simple expression' do
    parse(%q|(+ 2 4)|).should eq [[[:identifier, "+", [0, 1]],
                                   [:integer, 2, [0, 3]],
                                   [:integer, 4, [0, 5]]]]
    parse(%q|'a|).should eq [[[:idenitifer, "quote", [0, 0]],
                              [:identifier, "a", [0, 1]]]]
  end

  it 'parses integer literals' do
    parse(%q|24|).should eq [[:integer, 24, [0, 0]]]
  end
end

#'(foldl + 0 (range 0 2 7))'
#=>
#[[:lparen, '('], [:identifier, '('], [:identifier, '+'], [:integer, 0],
# [:lparen, '('], [:identifier, 'range'], [:integer, 0], [:integer, 2],
# [:integer, 7], [:rparen, ')'], [:rparen, ')']]
